<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body {
	font-family: Arial, Helvetica, sans-serif;
}

form {
	border: 3px solid #f1f1f1;
}

input[type=text], input[type=password] {
	width: 100%;
	padding: 12px 20px;
	margin: 8px 0;
	display: inline-block;
	border: 1px solid #ccc;
	box-sizing: border-box;
	border-radius: 50px;
}

button {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
	border-radius: 50px;
}

.reset {
	background-color: #4CAF50;
	color: white;
	padding: 14px 20px;
	margin: 8px 0;
	border: none;
	cursor: pointer;
	width: 100%;
	border-radius: 50px;
}

button:hover {
	opacity: 0.8;
}

.cancelbtn {
	width: auto;
	padding: 10px 18px;
	background-color: #f44336;
}

.imgcontainer {
	text-align: center;
	margin: 24px 0 12px 0;
}

img.avatar {
	width: 40%;
	border-radius: 50%;
}

.container {
	padding: 16px;
}

span.psw {
	float: right;
	padding-top: 16px;
}

.wrapper {
	margin: 0 auto;
}

.wrapper--w680 {
	max-width: 680px;
}
/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
	span.psw {
		display: block;
		float: none;
	}
	.cancelbtn {
		width: 100%;
	}
}
</style>
</head>
<body>

	<h1 style="text-align: center;">Login Form</h1>

	<div class="wrapper wrapper--w680">
		<form action="checkUser" method="post">
			<div class="container">
				<label for="uname"><b>Username</b></label> <input id="uname"
					type="text" placeholder="Enter Username" name="email" required>

				<label for="psw"><b>Password</b></label> <input id="pass"
					type="password" placeholder="Enter Password" name="password"
					required>
				<table style="width: 100%">
					<tr>
						<th width="50%"><button type="submit" onclick="loadDoc()">Login</button></th>
						<th><input type="reset" class="reset"></input></th>
					</tr>
					<tr>
						<td></td>
						<td><button type="submit"
								onclick="location.href='http://localhost:8080/register'">Register</button></td>
					</tr>

				</table>



			</div>
		</form>
	</div>
	<script>
	window.onload = function() {
		  test(${result});
		};
	function test(argument){
		if(argument==null){
			alert("Selamat Datang Di Login Page");		
		}else if(argument==1){
			alert("Username dan Password anda salah");			
		}else if(argument==2){
			alert("Password anda salah");
		}
		
	}
		
	</script>
</body>
</html>