package id.co.nexsoft.summative3;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.Query;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class WebController {
	@Autowired
	UserRepository repo;

	@GetMapping("/")
	public ModelAndView formLogin() {
		return new ModelAndView("index");
	}

	@PostMapping("/checkUser")
	public ModelAndView checkUser(User user) {
		User uname;
		String inputUser = user.getEmail();
		ModelAndView mv = new ModelAndView();
		if (repo.findByEmail(inputUser) == null && repo.findByPhone(inputUser) == null) {
			mv.addObject("result", 1);
			mv.setViewName("index");
			return mv;
		} else if (user.getEmail().matches(".*@.*")) {
			uname = repo.findByEmail(user.getEmail());
		} else {
			uname = repo.findByPhone(user.getEmail());
		}

		if (uname.getPassword().equals(user.getPassword())) {

			mv.setViewName("welcome");
			return mv;
		} else {
			// pass salah
			mv.addObject("result", 2);
			mv.setViewName("index");
			return mv;

		}

	}

	@GetMapping("/register")
	public ModelAndView formRegister() {
		return new ModelAndView("registration");
	}

	@PostMapping("/addUser")
	public ModelAndView addUser(User user) {
		ModelAndView mv = new ModelAndView();
		User databaseUser = repo.findByEmail(user.getEmail());
		if (databaseUser != null) {
			if (databaseUser.getEmail().equals(user.getEmail())) {
				mv.addObject("result", 1);
				mv.setViewName("registration");
				return mv;
			} else if (databaseUser.getPhone().equals(user.getPhone())) {
				mv.addObject("result", 2);
				mv.setViewName("registration");
				return mv;
			} else {
				mv.addObject("result", 3);
				mv.setViewName("registration");
				return mv;
			}
		}

		String specialCharacterRegex = "\\W";
		String phone = user.getPhone();
		phone = phone.replaceAll(specialCharacterRegex, "");
		int j = 0;
		String substringStr = null;
		if (phone.matches("0.*[0-9]")) {
			char[] array = phone.toCharArray();
			for (int i = 0; i < array.length; i++) {
				if (array[i] == 0) {
					j++;
				} else {
					break;
				}

			}
			String temp = phone.substring(j);
			String code = "+62";
			substringStr = code.concat(temp);
		}

		if (phone.substring(0, 2).matches("62")) {
			String plus = "+";
			substringStr = plus.concat(phone);
		}
		user.setPhone(substringStr);
		repo.save(user);
		return new ModelAndView("index");
	}
}
