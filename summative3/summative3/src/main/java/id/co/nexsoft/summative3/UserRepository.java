package id.co.nexsoft.summative3;

import org.springframework.data.repository.CrudRepository;



public interface UserRepository extends CrudRepository<User, Integer>{
	User findByEmail(String email);
	User findByPhone(String phone);
	User findByPassword(String phone);
}
